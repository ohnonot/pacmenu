#!/bin/sh

execpath="/usr/bin"
hookpath="/etc/pacman.d/hooks"

echo "This will install/update the executable to $execpath and the pacman hook to $hookpath"
read -p "Do you wish to continue [Y/n]? " x
case "$x" in
Y|y|'')
	[ ! -r "$execpath" ] && echo "$execpath is not readable. Aborting." && exit 1
	#~ [ -f "$execpath/pacmenu" ] && echo "$execpath/pacmenu already exists. Aborting." && exit 1
	sudo cp -v pacmenu "$execpath/pacmenu"
	[ ! -r "$hookpath" ] && echo "$hookpath is not readable. Not installing pacman hook." && exit 1
	sudo cp -v pacmenu.hook "$hookpath/pacmenu.hook"
;;
*)
	exit 1
;;
esac
