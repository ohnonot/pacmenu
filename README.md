## pacmenu
### Simple menu launcher that utilizes alpm data via [expac](http://github.com/falconindy/expac) (for archlinux-based distros)

    Usage: pacmenu [-m] [-d dir] [-l launcher] [-c config] [-t term] [-h] [-v]
    
    lists installed packages containing executables in $PATH, with description.
    Choosing a package presents a list of these executables, to choose & execute.
    
      -m Show the menu. This is the default if no options are given.
    
      -u Update cache. Won't show the menu unless specified with -m.
    
      -d Cache directory. Applies to -m and -u options. In it, the files packages
         and paths are created. Defaults to $cache_dir.
    
      -l Launcher command. Defaults to $launch_cmd.
    
      -c Read config file. Defaults to ~/.config/pacmenu/config. Sourced by bash.
         Command line options override config options.
    
      -t Terminal emulator for final command. Not given, the script uses $XTERMINAL,
         if defined. To force direct execution w/o terminal, supply an empty string.

### Quick Start:

`sudo ./install.sh` will copy the executable to `/usr/bin/` and the hook to
`/etc/pacman.d/hooks/`

`sudo pacmenu -u` to initialize the menu cache (from now on, the pacmenu.hook
automates this after every Install/Upgrade/Removal)

`pacmenu` to show the menu

### Usage:

You can search packages by typing keywords, e.g. `game`, `json`, `qt` and so on.
After choosing a package, a second menu appears with all the executables of that
package. Highlighting one & pressing enter will execute it the usual way.

If you highlight one, press \<Tab> and append a semicolon `;` to the line, then
enter, it will execute in a terminal. If you append two semicolons `;;` it will
hold the terminal open by executing the default `$SHELL` after the command.

Hint: some commands don't exit by themselves. If you opened them with a double 
semicolon, then terminating them with Ctrl+C closes the terminal, too! However,
I found that terminating them with Ctrl+D leaves the terminal open.

### Explanation:

The idea of pacmenu was to have a menu/launcher that sees all "programs",
i.e. executables, _with descriptions as to what they do_, and create an
applauncher/menu that is not depending on .desktop files. Currently the list is
sorted alphabetically by package names, with their description.  
Packages that don't have executables in `$PATH` are filtered out.

The script pipes its output to a generic menu application like dmenu. This is
configurable.

Additional grouping à la xdg-menu could be desired, but probably not practical
with dmenu. On the other hand, dmenu matches keywords anywhere in the short
description, so typing e.g. "game" should list most, if not all, games.

### Requirements:  

`bash awk sed expac` and a generic menu/launcher compatible with `dmenu`

### Config file

    # config options and their defaults
    
    #cache_dir="/var/cache/pacmenu"
    # command line option -c
    # Cache directory. Applies to -m and -u options. In it, the files packages
    # and paths are created.
    
    #launch_cmd="/usr/bin/dmenu -l 22"
    # command line option -l
    
    #terminal_cmd="$XTERMINAL"
    # command line option -t
    # Terminal emulator for final command. Not given, the script uses $XTERMINAL,
    # if defined. To force direct execution w/o terminal, supply an empty string.

An example is supplied with the package. If the script finds it in
`~/.config/pacmenu/config` it will source it.  
Command line options override config file options.

### Screenshots

First choice:  
![screenshot](http://dt.iki.fi/stuff/pacmenu/1.png)

Browsers maybe?
![screenshot](http://dt.iki.fi/stuff/pacmenu/1b.png)

Second choice:  
![screenshot](http://dt.iki.fi/stuff/pacmenu/2a.png)
