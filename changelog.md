### 0.1.1

#### Nov 8 2016

[Inspired by dmenu-extended][1] I solved the problem of not knowing whether a
command should be opened in a terminal or not: if a semicolon `;` is appended
to the command, it opens in a terminal. Two semicolons `;;` hold the terminal
open by executing the default `$SHELL` after the command.

I managed to apply the awk solution to the default update process, and it seems
to be more robust that way. The code looks messy now because it contains a
hardcoded config option, ann unused & broken function, and some commented-out
code, but this shouldn't have an effect on functionality.  
If this update method proves to be stable & faster, I will clean up the code.

[1]: https://github.com/markjones112358/dmenu-extended#-semi-colon---execute-in-terminal